# coding=utf8
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities

from libs import config

selenium_config = config['Selenium']


def driver_new_session():
    return webdriver.Remote(
        command_executor='http://127.0.0.1:9515',
        desired_capabilities=DesiredCapabilities.CHROME)


'''


def driver_new_session(proxy=False, show_image=True):
    options = webdriver.ChromeOptions()
    # options.add_argument('--allow-running-insecure-content')
    # options.add_argument('--disable-web-security')
    # options.add_argument('--no-referrers')

    if proxy:
        options.add_argument('--proxy-server=' + selenium_config['proxy'])

    if not show_image:
        options.add_argument("'chrome.prefs': {'profile.managed_default_content_settings.images': 2}")

    return webdriver.Remote(
        command_executor='http://{ip}:4444/wd/hub'.format(ip=selenium_config['host']),
        desired_capabilities=options.to_capabilities()
    )
'''

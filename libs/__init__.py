# coding=utf8
from logging.config import dictConfig
import yaml

from libs.help import get_root_path

root_path = get_root_path()

env_file = open(root_path + 'config.yaml', 'r')
config = yaml.safe_load(env_file.read())

# 设置logging
log_config = config['Log']
# 重构log path
for handle in log_config['handlers']:
    if 'filename' in log_config['handlers'][handle]:
        log_config['handlers'][handle]['filename'] = root_path + log_config['handlers'][handle]['filename']
dictConfig(log_config)
# logger = logging.getLogger(__name__)

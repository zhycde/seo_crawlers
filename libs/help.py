# coding=utf8
import os

import time


def get_root_path(sub='/'):
    path = os.path.split(os.path.realpath(__file__))[0]
    return os.path.realpath(path + '/../' + sub) + '/'


def str_to_time(str, format="%Y-%m-%d %H:%M:%S"):
    time_arr = time.strptime(str, format)
    return int(time.mktime(time_arr))


def time_to_str(format, unixtime=time.time()):
    time_arr = time.localtime(unixtime)
    return time.strftime(format, time_arr)


def get_pic(url):
    from io import BytesIO
    from PIL import Image
    import requests
    r = requests.get(url)
    return Image.open(BytesIO(r.content))


def get_num_str(str):
    import re
    pattern = re.compile(r'[1-9]\d*')
    searched = pattern.search(str)
    if searched is None:
        return 0
    else:
        return searched.group(0)

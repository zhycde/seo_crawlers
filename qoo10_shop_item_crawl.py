# coding=utf8
import os
import time
import tldextract

from libs.help import get_pic, get_num_str
from libs.sele import driver_new_session

from models.qoo10 import model_qoo10_item


def login(driver, suffix):
    username = 'zhycde'
    passwd = '123456Aa'
    if suffix == 'sg':
        home_page_url = 'https://my.qoo10.sg/gmkt.inc/Login/Login.aspx'
    elif suffix == 'my':
        home_page_url = 'https://my.qoo10.my/gmkt.inc/Login/Login.aspx'
    driver.get(home_page_url)

    driver.find_element_by_css_selector('#login_id').send_keys(username)
    driver.find_element_by_css_selector('#passwd').send_keys(passwd)
    # check_code_url = driver.find_element_by_css_selector('#qcaptcha_img').get_attribute('src')
    # check_code_img = get_pic(check_code_url)
    # driver.find_element_by_css_selector('#recaptcha_response_field').send_keys(image_to_string(check_code_img))
    input('enter code,after,press enter')
    driver.find_element_by_css_selector('#dv_member_login > span.login_btn_ara > a').click()

    return driver


def search(target):
    # driver = driver_new_session(proxy=True, show_image=False)

    suffix = tldextract.extract(target['url'])[2]

    # driver = login(driver, suffix)

    html_tpl = "<html><head></head><body><table>" \
               "<tr>" \
               "<th>idx</th><th>产品名</th><th>图</th><th>soldout</th><th>价格</th><th>链接</th>" \
               "</tr>" \
               "{trs}" \
               "</table></body></html>"
    tr_tpl = "<tr><td>{idx}</td><td>{title}</td><td>" \
             "<img src='{img}' style='width: 100px;'></td>" \
             "<td>{sold_out}</td><td>{price}</td><td>{href}</td></tr>"
    trs = []
    n = 0
    page = 0

    search_target = target['target']
    base_img_path = 'img/' + search_target + '/'
    if not os.path.exists('data/' + base_img_path):
        os.mkdir('data/' + base_img_path)

    # crawling
    # driver.get(target['url'])
    while True:
        page += 1
        print(page)
        for item in get_items2(suffix):
            img_path = item['img']
            try:
                i = get_pic(item['img'])
                img_path = base_img_path + str(n) + '.jpg'
                i.save('data/' + img_path)
            except:
                print("图片保存失败")

            trs.append(tr_tpl.format(
                idx=n,
                title=item['title'],
                img=img_path,
                sold_out=item['sold_out'],
                price=item['price'],
                href=item['url']
            ))
            n += 1
        break
        # next_a = driver.find_element_by_css_selector('#pageing_list > a.next')
        # if not next_a.get_attribute('href'):
        #    print('crawl finsh')
        #    break
        # time.sleep(5)
        # go next page
        # next_a.click()

    with open('data/' + search_target + '_qoo10_items.html', 'w', encoding='utf8') as f:
        f.write(html_tpl.format(trs="".join(trs)))
    print('write finsh')
    # driver.close()


def get_items2(suffix):
    last_id = None
    while True:
        res = model_qoo10_item.get_list(1000, last_id=last_id, where={'suffix': suffix})
        print(res.count(True))
        if res.count(True) < 1:
            break
        for item in res:
            yield item
            # print(item['_id'])
            last_id = item['_id']


def get_items(driver, suffix):
    dds = driver.find_elements_by_css_selector('#search_result_item_list dd')
    for dd in dds:
        goodscode = dd.get_attribute('id')
        item = model_qoo10_item.find_one({'goodscode': goodscode, 'suffix': suffix})
        if item is None:
            item = {}
            try:
                item['goodscode'] = goodscode
                item['title'] = dd.find_element_by_css_selector('td.details p').get_attribute('title')
                price = float(get_num_str(dd.find_element_by_css_selector('.price em').get_attribute('innerHTML')))
                discount = float(
                    get_num_str(dd.find_element_by_css_selector('.price span span').get_attribute('innerHTML')))
                item['price'] = price - discount
                item['img'] = dd.find_element_by_css_selector('td.thumb > div > a > img').get_attribute('gd_src')
                try:
                    sold_out = dd.find_element_by_css_selector(
                        'td.details.noInfo > div > div.optinfo > a.review.fs_11 > strong').get_attribute('innerHTML')
                except:
                    sold_out = 0
                item['sold_out'] = sold_out
                item['url'] = dd.find_element_by_css_selector('td.thumb > div > a').get_attribute('href')
                item['suffix'] = suffix
                model_qoo10_item.insert_one(item)
            except:
                print(item['title'])
                print("获取物品出问题")
                continue
        yield item


if __name__ == '__main__':
    urls = [
        # {
        #    'target': 'Sexual Wellness item', 'url':
        #    'http://list.qoo10.my/s/?gdlc_cd=100000007&gdmc_cd=200000231&gdsc_cd=&delivery_group_no=&sell_cust_no=&bundle_delivery=&bundle_policy=&keywordArrLength=1&keyword=&within_keyword_auto_change=&keyword_hist=Sexual+Wellness&image_search_rdo=U&attachFile=&search_image_url=&search_image_nm=&is_img_search_yn=N&sortType=SORT_RANK_POINT&dispType=LIST&flt_pri_idx=-1&filterDelivery=NNNNNANNNN&search_global_yn=&basis=&shipFromNation=&shipto=&brandnm=&SearchNationCode=&is_research_yn=Y&hid_keyword=Sexual+Wellness&orgPriceMin=&orgPriceMax=&priceMin=&priceMax=&curPage=1&pageSize=60&partial=off&brandno=&paging_value=1#anchor_detail_top'
        # },
        {
            'target': 'Sexual Wellness classify', 'url':
            'http://list.qoo10.sg/gmkt.inc/Category/Default.aspx?gdlc_cd=100000045&gdmc_cd=200000218&gdsc_cd=300000904&keywordArrLength=1&brand_keyword=&keyword_hist=&keyword=&within_keyword_auto_change=&image_search_rdo=U&attachFile=&search_image_url=&search_image_nm=&is_img_search_yn=N&sortType=SORT_RANK_POINT&dispType=LIST&flt_pri_idx=-1&filterDelivery=NNNNNANNNN&search_global_yn=&basis=&shipFromNation=&shipto=&brandnm=&SearchNationCode=&is_research_yn=Y&hid_keyword=&orgPriceMin=&orgPriceMax=&priceMin=&priceMax=&curPage=1&pageSize=120&partial=on&brandno=&paging_value=1'
        }
    ]
    for target in urls:
        search(target)

# coding=utf8
import json
import time

from libs.dbs import MongoBase
from libs.dbs import model_redis
from libs.sele import driver_new_session

CLASSIFY_KEY = 'SeoCrawl:classify'
model_weibo_trend = MongoBase('WeiboTrend')


def login():
    driver.get('http://weibo.com/login.php')
    driver.find_element_by_css_selector('#loginname').click()
    driver.find_element_by_css_selector('#loginname').send_keys('15162434911')
    driver.find_element_by_css_selector(
        '#pl_login_form > div > div:nth-child(3) > div.info_list.password > div > input').click()
    time.sleep(1)
    driver.find_element_by_css_selector(
        '#pl_login_form > div > div:nth-child(3) > div.info_list.password > div > input').send_keys(
        '940923zhyc')
    input('输入验证码后确定')
    driver.find_element_by_css_selector('#pl_login_form > div > div:nth-child(3) > div.info_list.login_btn > a').click()
    print('登录成功')


def get_classiyf():
    classify = model_redis.get(CLASSIFY_KEY)
    if not classify:
        classify = {}
        driver.get('http://d.weibo.com/100803?refer=index_hot_new')
        time.sleep(5)
        classify_a = driver.find_elements_by_css_selector('.ul_item a')
        for a in classify_a:
            keyword = a.find_element_by_css_selector('.item_title').get_attribute('innerHTML')
            if keyword == '更多' or keyword == '我的':
                continue
            classify.update({
                keyword: a.get_attribute('href')
            })
        model_redis.set(CLASSIFY_KEY, json.dumps(classify))
    else:
        classify = json.loads(classify)
    return classify


def get_last_other(classify, url):
    driver.get(url)
    trend = []
    return trend


def get_last_all(url):
    driver.get(url)
    trend = []
    for i in range(1, 5):
        print("第{i}页/4".format(i=i))
        for li in driver.find_elements_by_css_selector('.pt_ul li'):
            title_a = li.find_element_by_css_selector('.info_box .title > a')
            tag = li.find_element_by_css_selector(
                '.info_box > div.text_box > div.title.W_autocut > a.W_btn_b.W_btn_tag').text
            read_num = li.find_element_by_css_selector(
                'div.info_box > div.subinfo.clearfix > div:nth-child(1) > span > span > span').text
            host_a = li.find_element_by_css_selector(
                'div.info_box > div.subinfo.clearfix > div:nth-child(2) > span > span > a')
            trend.append({
                'Title': title_a.text,
                'Link': title_a.get_attribute('href'),
                'Tag': tag,
                'ReadNum': read_num,
                'Host': {
                    'Name': host_a.text,
                    'Link': host_a.get_attribute('href')
                }
            })
        driver.find_element_by_css_selector('a.page.next.S_txt1.S_line1').click()
        time.sleep(5)
    return trend


if __name__ == '__main__':
    driver = driver_new_session()
    login()

    classify_list = get_classiyf()
    print('可选择的分类：' + ' '.join(classify_list.keys()))

    classify = '话题榜'
    print('当前分类：{}'.format(classify))
    if classify == '话题榜':
        classify_trend = get_last_all(classify_list[classify])
    else:
        classify_trend = get_last_other(classify, classify_list[classify])
    model_weibo_trend.insert_one({
        'Classify': classify,
        'TimeStamp': int(time.time()),
        'Trend': classify_trend
    })

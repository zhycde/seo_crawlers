# coding=utf8

from models.drugs import model_drug_item


def get_items():
    last_id = None
    while True:
        res = model_drug_item.get_list(1000, last_id=last_id)
        print(res.count(True))
        if res.count(True) < 1:
            break
        for item in res:
            yield item
            # print(item['_id'])
            last_id = item['_id']


if __name__ == '__main__':
    res = ['品名,价格,发货地,送达地,描述']
    for item in get_items():
        res.append(
            ','.join([
                item['Title'].replace(',', '.'), item['Price'], item['ShipsFrom'], item['ShipsTo'],
                item['Description'].replace(',', '，').replace('\n', '')
            ])
        )
    with open("data/drug.csv", 'w', encoding='utf-8-sig') as f:
        f.write(
            '\n'.join(res)
        )

# coding=utf8
from models.fb import model_fb_user

csv = ['姓名,Message号,性别,性取向,职业/公司,教育背景,故乡,目前所在地,婚姻状况']

for user in model_fb_user.find():
    message_id = user['HomePage'].split('/')[3]
    user_detail = [user['Name'], message_id, user['Basic']['Sex'] if 'Sex' in user['Basic'] else '']
    if 'Sexuality' in user['Basic']:
        user_detail.append(user['Basic']['Sexuality'])
    else:
        user_detail.append('')
    if 'Work' in user:
        user_detail.append(user['Work'][0].replace(',', '，'))
    else:
        user_detail.append('')
    if 'Education' in user and len(user['Education']) > 0:
        user_detail.append(user['Education'][0].replace(',', '，'))
    else:
        user_detail.append('')
    if 'Places' in user:
        if 'Hometown' in user['Places']:
            user_detail.append(user['Places']['Hometown'].replace(',', '，'))
        else:
            user_detail.append('')
        if 'Current' in user['Places']:
            user_detail.append(user['Places']['Current'].replace(',', '，'))
        else:
            user_detail.append('')
    if 'MaritalStatus' in user:
        user_detail.append(user['MaritalStatus'])
    else:
        user_detail.append('')
    csv.append(','.join(user_detail))

with open("data/fb_froup.csv", 'w', encoding='utf-8-sig') as f:
    f.write("\n".join(csv))

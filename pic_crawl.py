# coding=utf8
import time
import json

from libs.sele import driver_new_session
from models.ggpic import model_gg_pic


def search_keyword(keyword):
    search_url = "https://www.google.co.jp/imghp?hl=zh-CN&tab=wi&ei=XA9yV7KlHcWJmQHYop1A&ved=0EKouCBIoAQ"
    driver = driver_new_session()
    driver.get(search_url)
    driver.find_element_by_id('lst-ib').send_keys(keyword)
    driver.find_element_by_id('sbds').click()

    for pic in get_pics(driver):
        model_gg_pic.insert_one(pic)

    driver.close()


def get_pics(driver):
    def load_more(n):
        # 滚动到底部自动加载
        heigth = 15000
        driver.execute_script("window.scrollTo({start}, {end});".format(start=heigth * n, end=heigth * (n + 1)))

    old_total = 0
    repeat_num = 0
    n = 0
    while True:
        pics = driver.find_element_by_id('rg_s').find_elements_by_css_selector('div.rg_di.rg_el.ivg-i')
        total = len(pics)
        print(total)
        if old_total == total:
            repeat_num += 1
            if repeat_num > 3:
                print("搜索完毕")
                break
        old_total = total
        load_more(n)
        n += 1
        time.sleep(4)

    for pic in driver.find_element_by_id('rg_s').find_elements_by_css_selector('div.rg_di.rg_el.ivg-i'):
        pic_data_str = pic.find_element_by_class_name('rg_meta').get_attribute('innerHTML')
        pic_data = json.loads(pic_data_str)
        print(pic_data['s'])
        # 暂时不记录其他字段
        yield {
            'Ext': pic_data['ity'],
            'Url': pic_data['ou']
        }


if __name__ == '__main__':
    # gg这里也是ajax加载的= =
    search_keyword('happy')

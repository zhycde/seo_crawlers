# coding=utf8
import time

from libs.sele import driver_new_session


def search_keyword(url):
    driver = driver_new_session()
    driver.get(url)

    res = ["商品名,评论数"]
    for item in get_elem(driver):
        res.append(item)

    with open("data/amazon_shop_crawl.csv", 'w', encoding='utf-8-sig') as f:
        f.write("\n".join(res))

    driver.close()


def get_elem(driver):
    n = 0
    while True:
        n += 1
        print(n)
        time.sleep(3)
        driver.execute_script('document.body.scrollTop = document.body.scrollHeight')
        time.sleep(3)
        lis = driver.find_elements_by_css_selector('#s-results-list-atf li')
        for li in lis:
            try:
                title = li.find_element_by_css_selector('.a-spacing-top-mini a').get_attribute('title').replace(',',
                                                                                                                '，')
            except:
                continue
            try:
                comm_num = li.find_element_by_css_selector('.a-spacing-top-micro a.a-link-normal').get_attribute(
                    'innerHTML')
            except:
                comm_num = 0
            yield title + "," + str(comm_num)
        try:
            pn = driver.find_element_by_css_selector('a#pagnNextLink')
            pn.click()
        except:
            break


if __name__ == '__main__':
    search_keyword('https://www.amazon.com/s/ref=sr_pg_1?rh=k%3ASexual+Remedies%2Cn%3A3760901&keywords='
                   'Sexual+Remedies&ie=UTF8&qid=1468210138&spIA=B00O82ONB8,B008KEHA2E,B01446NIMC,B01FA0CABQ,'
                   'B00YQKKKRS,B01A5T51LS,B01DR6EHL6,B009GZCXJG,B01A09JXNK')

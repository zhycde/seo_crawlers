# coding=utf8
import json
import time
from urllib.parse import quote

import requests


def get_baidu_list(keyword):
    surl = "https://sp0.baidu.com/5a1Fazu8AA54nxGko9WTAnF6hhy/su?wd={wd}&json=0&_={time}"
    url = surl.format(wd=quote(keyword), time=int(time.time() * 1000))
    r = requests.get(url)
    un_json_data = r.text.replace('window.baidu.sug(', '').replace(');', '')
    json_data = un_json_data.replace("q:", '"q":').replace("p:", '"p":').replace("s:", '"s":')
    re_data = json.loads(json_data)
    return re_data['s']


def get_gg_list(keyword, hl='zh-CN'):
    surl = "https://www.google.co.jp/complete/search?client=hp&hl={hl}&gs_rn=64&gs_ri=hp&cp=6&gs_id=kx&q={wd}&xhr=t"
    url = surl.format(wd=quote(keyword), hl=hl)
    proxies = {
        'http': 'socks5://127.0.0.1:10808',
        'https': 'socks5://127.0.0.1:10808',
    }
    r = requests.get(url, proxies=proxies)
    re_data = json.loads(r.text)
    re = list()
    for item in re_data[1]:
        re.append(item[0])
    return re


def get_taobao_list(keyword):
    surl = 'https://suggest.taobao.com/sug?code=utf-8&q={wd}&_ksTS={time}&k=1&area=c2c&bucketid=17'
    url = surl.format(wd=quote(keyword), time=int(time.time() * 1000))
    r_json = requests.get(url).json()
    re = []
    for item in r_json['result']:
        re.append(item[0])
    return re


if __name__ == '__main__':
    import sys

    # lang_list = ['zh-CN', 'en-CH', 'ja']
    lang_list = ['zh-CN']
    keyword_list = []
    if len(sys.argv) >= 2:
        keyword_list = sys.argv[1:]
    for keyword in keyword_list:
        with open("data/{keyword}_baidu.csv".format(keyword=keyword), 'w', encoding="utf-8-sig") as f:
            f.write("\n".join(get_baidu_list(keyword)))
        with open("data/{keyword}_taobao.csv".format(keyword=keyword), 'w', encoding="utf-8-sig") as f:
            f.write("\n".join(get_taobao_list(keyword)))
        for lang in lang_list:
            with open("data/{keyword}_{lang}_gg.csv".format(keyword=keyword, lang=lang), 'w',
                      encoding="utf-8-sig") as f:
                f.write("\n".join(get_gg_list(keyword)))
